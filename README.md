# Parallel File Copy Program

## Overview
This program is written in the `C` programming language. It copies all the *regular* files from the source directory to the destination directory using a contant number of [pthreads](https://computing.llnl.gov/tutorials/pthreads/).

## CMI Documentation
`./main <source> <destination> <threads>`