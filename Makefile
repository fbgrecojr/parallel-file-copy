all: main

main: main.o copyfilepass.o restart.o
	gcc main.o copyfilepass.o restart.o -pthread -o main

main.o: main.c
	gcc -Wall -c main.c

copyfilepass.o: copyfilepass.c
	gcc -Wall -c copyfilepass.c

restart.o: restart.c
	gcc -Wall -c restart.c	

clean:
	rm *.o
	rm main
