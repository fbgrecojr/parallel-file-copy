#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include "copyfilepass.h"

#define R_FLAGS O_RDONLY
#define W_FLAGS (O_WRONLY | O_CREAT)
#define W_PERMS (S_IRUSR | S_IWUSR)
#define KILOBYTE 0.001

int main(int argc, char* argv[]){

	int num_threads;		//number of threads to execute in parallel
	int cur_threads = 0;	//number of threads currently executing
	DIR* source;			//pointer to the source directory
	DIR* dest;				//pointer to the destination directory
	struct dirent* dentry;	//pointer to the internal structure of the source directory
	struct stat fst;		//stats for each file in a directory
	int error;				//will hold an error
	void *status;			//holds status of pthread_join
	pthread_t *threads;
	int oldest_thread = 0;

	//***BEGIN ERROR CHECKING***

	if (argc != 4) {
		fprintf(stderr, "Usage: %s sourceDir destDir numThreads\n", argv[0]);
		return 1; 
	}

	//check if we can successfully open the source directory
	if( (source = opendir(argv[1])) == NULL ) {
		fprintf(stderr, "Error opening source directory %s\n", argv[1]); 
		return 1;
	}

	//check if we can successfully open the destination directory
	if( (dest = opendir(argv[2])) == NULL ) {
		fprintf(stderr, "Error opening destination directory %s\n", argv[2]); 
		return 1;
	}

	num_threads = atoi(argv[3]);

	if ( (threads = (pthread_t *)calloc(num_threads, sizeof(pthread_t))) == NULL ) {
		perror("Failed to allocate copier space");
		return 1;
	}  

	if(num_threads < 1) {
		printf("There are not threads(%d) available to preform copy\n", num_threads);
		return 1;
	}

	//***END ERROR CHECKING***

	while( (dentry = readdir(source)) != NULL ){
		//source path
		char* path = (char*)malloc(sizeof(char) * (strlen(dentry->d_name) + strlen(argv[1]) + 2)); //need '.' + '/' + '\0'
		sprintf(path, "%s%c%s", argv[1], '/', dentry->d_name);

		//destination path
		char* dest_path = (char*)malloc(sizeof(char) * (strlen(dentry->d_name) + strlen(argv[2]) + 2)); //need '.' + '/' + '\0'
		sprintf(dest_path, "%s%c%s", argv[2], '/', dentry->d_name);

		if(!stat(path, &fst)){ //stat() return 0 if successful
			if(S_ISREG(fst.st_mode)){

				int args[3];

				if ( (args[0] = open(path, R_FLAGS)) == -1 ) {
					fprintf(stderr, "Failed to open source file %s: %s\n", path, strerror(errno));
					continue; 
				}
				if ( (args[1] = open(dest_path, W_FLAGS, W_PERMS)) == -1 ) {
					fprintf(stderr, "Failed to open destination file %s: %s\n", dest_path, strerror(errno));
					continue;
				}

				if( cur_threads < num_threads ) {
					if ( (error = pthread_create((&threads[cur_threads++]), NULL, copyfilepass, args)) ) {
						--cur_threads;
						fprintf(stderr, "Failed to create thread: %s\n", strerror(error));
						threads[cur_threads++] = pthread_self(); 
						continue;
					}
					printf("file: %.03fKB %s\n", (fst.st_size * KILOBYTE), path);
				} else{
					//wait for oldest thread to complete
					if ( (error = pthread_join(threads[oldest_thread % num_threads], &status)) ) {
						fprintf(stderr, "Failed to join thread\n");
						continue;
					}

					//then since we have a thread free, create a new thread
					if ( (error = pthread_create((&threads[oldest_thread++ % num_threads]), NULL, copyfilepass, args)) ) {
						--oldest_thread;
						fprintf(stderr, "Failed to create thread: %s\n", strerror(error));
						threads[oldest_thread++ % num_threads] = pthread_self(); 
						continue;
					}
					printf("file: %.03fKB %s\n", (fst.st_size * KILOBYTE), path);
				}
			}
		}
		free(path);
		free(dest_path);
	}

	//join all threads
	for(int i=0; i<cur_threads; ++i) {
		if (pthread_equal(threads[i], pthread_self())){
			continue;
		}
		if ( (error = pthread_join(threads[i], &status)) ) {
			fprintf(stderr, "Failed to join thread %d\n", i);
			continue;
		}
	}


	//close directory
	closedir(source);

	return 0;

}
